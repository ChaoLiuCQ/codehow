Introduction
============
Within this archive you find the replication package of CodeHow for the article "Simplifying Deep-Learning-Based Model for Code Search" by Chao Liu, Xin Xia, David Lo, Ahmed E. Hassan, and Shanping Li for currently under review at ACM Transactions on Software Engineering and Methodology. The aim of this replication package is to allow other researchers to replicate our results with minimal effort, as well as to provide additional results that could not be included in the article directly.


Data Contents
========

Data download: get data from the [link](https://bitbucket.org/ChaoLiuCQ/codebase) 

codebase: the testing data and related model data

data: model related data

script: python scripts for the model



How to use
==========
Data download: https://pan.baidu.com/s/1mLFgV7Tk10wz2bP3VONpQg with permanent key(i84e)

Install Elasticsearch: https://www.elastic.co

Create index and fill data into the Elasticsearch: create_index() and fill_data() in script/elasticsearch.py 

Perform code search: queries_search() in script/elasticsearch.py 
