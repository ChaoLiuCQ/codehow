import re
import multiprocessing
import os
import util
from api_understanding import ApiUnderstanding
import pandas as pd
from gensim import corpora, models

total_files = 41025
n_threads = 50

path_from = '../codebase/janalyzer/'
path_to = '../codebase/'


def multi_method():
    pool = multiprocessing.Pool(processes=n_threads)
    for i in range(total_files):
        print('method: ' + str(i))
        pool.apply_async(code_method, (i,))
    pool.close()
    pool.join()


def code_method(idx):
    file = path_from + 'file_' + str(idx) + '_Method.csv'
    if not os.path.exists(file):
        return
    code = util.load_txt(file)

    for i in range(len(code)):
        line = str(code[i])
        line = line[line.index(';') + 1:]
        line = line.split(',')
        code[i] = line[1] + '.' + line[2] + '\n'

    with open(path_to + 'raw_fqn/fqn' + str(idx) + '.txt', 'w', encoding='utf-8') as infile:
        infile.writelines(code)
    print('method over: ' + str(idx))


def multi_body():
    pool = multiprocessing.Pool(processes=n_threads)
    for i in range(total_files):
        print('method: ' + str(i))
        pool.apply_async(code_body, (i,))
    pool.close()
    pool.join()


def code_body(idx):
    file = path_from + 'file_' + str(idx) + '_Source.csv'
    if not os.path.exists(file):
        return

    body = util.load_txt(file)
    for i in range(len(body)):
        line = str(body[i])
        line = line[line.index(';') + 1:]
        line = re.sub(' +', ' ', line).strip()
        line = re.sub(';+', ';', line)
        body[i] = line + '\n'

    with open(path_to + 'body/body' + str(idx) + '.txt', 'w', encoding='utf-8') as infile:
        infile.writelines(body)
    print('method over: ' + str(idx))


def multi_parsed():
    pool = multiprocessing.Pool(processes=n_threads)
    for i in range(total_files):
        print('parsed: ' + str(i))
        pool.apply_async(code_parsed, (i,))
    pool.close()
    pool.join()


def code_parsed(idx):
    file = path_from + 'file_' + str(idx) + '_ParsedCode.csv'
    if not os.path.exists(file):
        return

    code_parsed = util.load_txt(file)

    for i in range(len(code_parsed)):
        line = str(code_parsed[i])
        line = line[line.index(';') + 1:-1]
        body = []
        if ';' in line:
            line = line.split(';')
            for j in range(len(line)):
                l = line[j].split(',')
                if len(l) == 3:
                    api = str(l[2])
                    if api.endswith('()'):
                        body.append(api[:-2])
        if len(body) == 0:
            body = '[]'
        else:
            body = ','.join(body)
        code_parsed[i] = body + '\n'

    with open(path_to + 'apis/apis' + str(idx) + '.txt', 'w', encoding='utf-8') as infile:
        infile.writelines(code_parsed)
    print('parsed over: ' + str(idx))


def multi_source():
    pool = multiprocessing.Pool(processes=n_threads)
    for i in range(total_files):
        print('source: ' + str(i))
        pool.apply_async(code_source, (i,))
    pool.close()
    pool.join()


def code_source(idx):
    file = path_from + 'file_' + str(idx) + '_SourceCode.csv'
    if not os.path.exists(file):
        return
    code_source = util.load_txt(file)
    for i in range(len(code_source)):
        line = str(code_source[i])
        line = line[line.index(';') + 1:]
        if '[]' == line:
            code_source[i] = '[]\n'
        else:
            sub = line.split(';')
            cmt = ''
            for s in sub:
                if s == '':
                    cmt += ';'
                    continue
                s = s.strip()
                if not s.startswith('//') and not s.startswith('/*') and not s.startswith('*') and \
                        not s.startswith('@') and not s.endswith('*') and not s.endswith('*/'):
                    cmt += s
            cmt = re.sub(' +', ' ', cmt).strip()
            cmt = re.sub(';+', ';', cmt)
            code_source[i] = cmt + '\n'

    with open(path_to + 'raw_code/code' + str(idx) + '.txt', 'w', encoding='utf-8') as infile:
        infile.writelines(code_source)
    print('source over: ' + str(idx))


def multi_source_parse():
    pool = multiprocessing.Pool(processes=30)
    for i in range(total_files):
        print('method: ' + str(i))
        pool.apply_async(code_source_parse, (i,))
    pool.close()
    pool.join()


def code_source_parse(idx):
    file = '../codebase/raw_code/code' + str(idx) + '.txt'
    if not os.path.exists(file):
        return
    lines = util.load_txt(file)
    data = pd.DataFrame(lines, columns=['code'])
    data = data['code'].apply(au.preprocessing)
    data.to_pickle('../codebase/parsed_code/code' + str(idx) + '.pkl')
    print(idx)


def multi_method_parse():
    pool = multiprocessing.Pool(processes=30)
    for i in range(total_files):
        print('method: ' + str(i))
        pool.apply_async(code_method_parse, (i,))
    pool.close()
    pool.join()


def code_method_parse(idx):
    file = '../codebase/raw_fqn/fqn' + str(idx) + '.txt'
    if not os.path.exists(file):
        return
    lines = util.load_txt(file)
    data = pd.DataFrame(lines, columns=['fqn'])
    data = data['fqn'].apply(au.preprocessing)
    data.to_pickle('../codebase/parsed_fqn/fqn' + str(idx) + '.pkl')


def build_tfidf():
    dictionary = corpora.Dictionary([])
    for idx in range(41025):
        print('dictionary: ' + str(idx))
        file = '../codebase/parsed_code/code' + str(idx) + '.pkl'
        if not os.path.exists(file):
            continue
        dictionary.add_documents(util.load_pkl(file))
    util.save_pkl('../codebase/dictionary.pkl', dictionary)

    text_bow = []
    for idx in range(41025):
        print('text_bow: ' + str(idx))
        file = '../codebase/parsed_code/code' + str(idx) + '.pkl'
        if not os.path.exists(file):
            continue
        code = list(util.load_pkl(file))
        text_bow.extend([dictionary.doc2bow(s) for s in code])
    util.save_pkl('../codebase/text_bow.pkl', text_bow)

    text_bow = util.load_pkl('../codebase/text_bow.pkl')
    print('build ...')
    tfidf_model = models.TfidfModel(text_bow)
    print('save ...')
    util.save_pkl('../codebase/cr_vsm.pkl', tfidf_model)


au = ApiUnderstanding()

if __name__ == '__main__':
    # extract raw_code and raw_fqn from the raw data (codebase/janalyzer)
    multi_method()
    multi_source()

    # parse raw_code and raw_fqn into the parsed_code and parsed_fqn
    multi_method_parse()
    multi_source_parse()

    # build tfidf model with outputs: dictonary.pkl, text_bow.pkl, and cr_vsm.pkl
    build_tfidf()
    print('done')
