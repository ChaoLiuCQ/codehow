def sim_q_text_d(query, d, method_name, idx2tfidf, token2id):
    w_t_q = []
    w_t_d = []

    for token in query:
        if token in token2id:
            if token in method_name:
                w_t_q.append(1.5)
            else:
                w_t_q.append(1.)
            if token in d:
                w_t_d.append(0.5 + 0.5 * idx2tfidf[token2id[token]])
            else:
                w_t_d.append(0)
    if len(w_t_q) == 0:
        raise RuntimeError('query token: ' + str(query))
    sim = sim_q_and_d(w_t_q, w_t_d)
    return sim


def sim_q_api_d(query, api, api_score, d, method_name, idx2tfidf, token2id):
    w_t_q = []
    w_t_d = []

    for token in query:
        if token in token2id:
            if token in method_name:
                w_t_q.append(1.5)
            elif token in api:
                w_t_q.append(1.5)
            else:
                w_t_q.append(1.)

            if token in api:
                w_t_d.append(api_score)
            elif token in d:
                w_t_d.append(0.5 + 0.5 * idx2tfidf[token2id[token]])
            else:
                w_t_d.append(0)
    if len(w_t_q) == 0:
        raise RuntimeError('query token: ' + str(query))
    sim = sim_q_and_d(w_t_q, w_t_d)
    return sim


def sim_q_and_d(w_t_q, w_t_d, p=3):
    numerator = 0.
    denominator = 0.
    for i in range(len(w_t_q)):
        q_p = w_t_q[i] ** p
        _1_d_p = (1 - w_t_d[i]) ** p
        numerator += q_p * _1_d_p
        denominator += q_p
    base = numerator / denominator
    if base < 0:
        base = -base
        sim = 1 + base ** (1 / p)
    else:
        sim = 1 - base ** (1 / p)
    return sim


def sim_q_expand_d(query, api_list, d, method_name, idx2tfidf, token2id):
    if len(set(query) & set(d)) == 0:
        return 0.
    sim = 0.
    for api, api_score in api_list:
        sim += sim_q_api_d(query, api, api_score, d, method_name, idx2tfidf, token2id)
    sim += sim_q_text_d(query, d, method_name, idx2tfidf, token2id)
    return sim