from gensim import corpora, models, similarities
import pickle


class VectorSpaceModel:
    def __init__(self):
        self.tfidf_model = None
        self.dictionary = None
        self.text_bow = None
        self.name_bow = None

    def build_tfidf_model(self, text_corpus, name_corpus):
        self.dictionary = corpora.Dictionary(text_corpus)
        self.text_bow = [self.dictionary.doc2bow(s) for s in text_corpus]
        self.name_bow = [self.dictionary.doc2bow(s) for s in name_corpus]
        self.tfidf_model = models.TfidfModel(self.text_bow)

    def get_tfidf(self, tokens):
        token_bow = self.dictionary.doc2bow(tokens)
        tfidf = self.tfidf_model[token_bow]
        return tfidf

    def calculate_cosine_similarity(self, token_tfidf, matching):
        if matching == 'text':
            index = similarities.SparseMatrixSimilarity(self.tfidf_model[self.text_bow],
                                                        num_features=len(self.dictionary.keys()))
        elif matching == 'name':
            index = similarities.SparseMatrixSimilarity(self.tfidf_model[self.name_bow],
                                                        num_features=len(self.dictionary.keys()))
        else:
            raise RuntimeError()
        sim = index[token_tfidf]
        return sim

    def save(self, path='../data/vsm.pkl'):
        with open(path, 'wb') as f:
            pickle.dump([self.tfidf_model, self.dictionary, self.text_bow, self.name_bow], f)

    def load(self, path='../data/vsm.pkl'):
        with open(path, 'rb') as f:
            self.tfidf_model, self.dictionary, self.text_bow, self.name_bow = pickle.load(f)
