from elasticsearch import Elasticsearch
from elasticsearch import helpers
import os
import time
import util
import code_retrieval as cr
from api_understanding import ApiUnderstanding


class SearchEngine:
    def __init__(self):
        self.index_name = "codehow"
        self.doc_type = "code"
        self.ip = "localhost:9200"
        self.es = Elasticsearch([self.ip])
        self.id = 0

    def create_index(self):
        body = {"mappings": {self.doc_type: {"properties": {
            "parsed_code": {"type": "text"},
            "parsed_fqn": {"type": "text"},
            "raw_code": {"type": "text"}
        }}}}
        self.es.indices.create(index=self.index_name, body=body)

    def delete_index(self, index_name):
        self.es.indices.delete(index_name)

    def format_data(self, path_from, path_to, total_files, split_size):
        body = list()
        count = 0
        index = 0
        for i in range(total_files):
            print(str(i))
            parsed_code = []
            if os.path.exists(path_from + 'parsed_code/code' + str(i) + '.pkl'):
                parsed_code = util.load_pkl(path_from + 'parsed_code/code' + str(i) + '.pkl')

            parsed_fqn = []
            if os.path.exists(path_from + 'parsed_fqn/fqn' + str(i) + '.pkl'):
                parsed_fqn = util.load_pkl(path_from + 'parsed_fqn/fqn' + str(i) + '.pkl')

            raw_code = []
            if os.path.exists(path_from + 'raw_code/code' + str(i) + '.txt'):
                raw_code = util.load_txt(path_from + 'raw_code/code' + str(i) + '.txt')

            for j in range(len(raw_code)):
                pc = ' '.join(list(parsed_code[j]))
                pf = ' '.join(list(parsed_fqn[j]))
                rc = str(raw_code[j])

                body.append({
                    "_index": self.index_name,
                    "_type": self.doc_type,
                    "_source": {
                        "parsed_code": pc,
                        "parsed_fqn": pf,
                        "raw_code": rc
                    }
                })

            count += 1
            if count == split_size:
                util.save_pkl(path_to + 'data' + str(index) + '.pkl', body)
                index += 1
                count = 0
                body = list()
        util.save_pkl(path_to + 'data' + str(index) + '.pkl', body)

    def fill_data(self, path_from):
        for i in range(10):
            print(str(i) + '-9')
            body = util.load_pkl(path_from + 'data' + str(i) + '.pkl')
            helpers.bulk(self.es, body)

    def search(self, q_pre, api_list, idx2tfidf, i, min_should_match):
        query = {"query": {"match": {"raw_code": {"query": ' '.join(q_pre), "minimum_should_match": min_should_match}}}}
        scanResp = helpers.scan(self.es, query, index=self.index_name, scroll="10m")
        top10 = []
        count = 0
        for hit in scanResp:
            code = hit['_source']
            raw_code = code['raw_code']
            parsed_code = code['parsed_code'].split(' ')
            parsed_fqn = code['parsed_fqn'].split(' ')
            sim = cr.sim_q_expand_d(q_pre, api_list, parsed_code, parsed_fqn, idx2tfidf, token2id)
            if len(top10) <= 9:
                top10.append([sim, raw_code])
                if len(top10) == 10:
                    top10 = sorted(top10, key=lambda x: x[0], reverse=True)
            else:
                if sim > top10[-1][0]:
                    top10[-1] = [sim, raw_code]
                    top10 = sorted(top10, key=lambda x: x[0], reverse=True)
            count += 1
            print(str(i) + ': ' + str(count) + '-' + str(top10[0][0]) + '-' + str(top10[-1][0]))
        return top10

    def queries_search(self, path_queries):
        queries = util.load_csv(path_queries)
        for i in range(3, 4):
            q = queries[i][0]
            q_pre = au.preprocessing(q)
            query_tfidf = get_tfidf(q_pre)
            idx2tfidf = {}
            for tid, tfidf in query_tfidf:
                idx2tfidf[tid] = tfidf

            api_top_k = au.identify_relevant_api(q)
            api_list = []
            for api_idx, api_score in api_top_k:
                api = au.method_list[api_idx]['fqn']
                api_list.append([api, api_score])

            print(i)
            top10 = self.search(q_pre, api_list, idx2tfidf, i, 2)
            if len(top10) < 10:
                top10.extend(self.search(q_pre, api_list, idx2tfidf, i, 1))
            top10 = sorted(top10, key=lambda x: x[0], reverse=True)
            top10 = top10[:10]

            with open('../codebase/search/search' + str(i) + '.txt', 'w', encoding='utf-8') as writer:
                writer.write(str(i) + ', ' + q + '\n')
                for j in range(len(top10)):
                    writer.write(str(j) + ', ' + str(top10[j][0]) + ', ' + str(top10[j][1]) + '\n')


au = ApiUnderstanding()
tfidf_model = util.load_pkl('../codebase/cr_vsm.pkl')
dictionary = util.load_pkl('../codebase/dictionary.pkl')
token2id = dictionary.token2id


def get_tfidf(tokens):
    token_bow = dictionary.doc2bow(tokens)
    tfidf = tfidf_model[token_bow]
    return tfidf


if __name__ == '__main__':
    now = time.time()

    se = SearchEngine()

    # delete existing index
    se.delete_index('codehow')

    # create new index
    se.create_index()

    # format codebase into required formats
    se.format_data(path_from='../codebase/',
                   path_to='../codebase/elasticsearch/',
                   total_files=41025,
                   split_size=4000)

    # fill formatted codebase into elasticsearch
    se.fill_data('../codebase/elasticsearch/')

    # perform search with queries on indexed codebase
    se.queries_search('../data/queries.csv')

    later = time.time()
    diff = later - now
    print(diff)
