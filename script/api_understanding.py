import os
import pickle
import re

import jsonlines as jl
import nltk
import numpy as np
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

from vector_space_model import VectorSpaceModel


class ApiUnderstanding:
    def __init__(self):
        try:
            nltk.data.find('corpora/stopwords.zip')  # 检查stopwords资源是否已下载
        except LookupError:
            nltk.download('stopwords')

        self.vsm = VectorSpaceModel()
        self.stemmer = PorterStemmer()
        self.method_list = []
        self.stopwords = stopwords.words('english')

        if not os.path.exists('../data/au.pkl'):
            self.build_model('../data/method.jsonl')
            self.save()
        else:
            self.load()

    def save(self, path='../data/au.pkl'):
        with open(path, 'wb') as f:
            pickle.dump(self.method_list, f)
        self.vsm.save()

    def load(self, path='../data/au.pkl'):
        with open(path, 'rb') as f:
            self.method_list = pickle.load(f)
        self.vsm.load()

    @staticmethod
    def remove_punctuation(s):
        p = re.compile(r'([a-z]|\d)([A-Z])')
        res = re.sub(p, r'\1_\2', s)
        res = re.sub(r'[\W_]+', ' ', res).lower().strip()
        return res

    @staticmethod
    def get_top_k_index(l, k):
        arr = np.array(l)
        ind = arr.argpartition(-k)[-k:]  # 找出top k所在下标
        ind = ind[np.argsort(arr[ind])]  # 升序排序
        ind = ind[::-1]  # 反转，变为降序
        return ind

    def preprocessing(self, s):
        if s is '':
            return []
        s = self.remove_punctuation(s)
        word_list = s.split(' ')
        # stemming & remove stop words
        clean_word_list = [self.stemmer.stem(word) for word in word_list if word not in self.stopwords]
        return clean_word_list

    def text_matching(self, query_tfidf, k=10):
        sim = self.vsm.calculate_cosine_similarity(query_tfidf, matching='text')
        top_k_idx = self.get_top_k_index(sim, k)
        text_top_k = {}
        for idx in top_k_idx:
            text_top_k[idx] = [sim[idx], -1]
        return text_top_k

    def name_matching(self, query_tfidf, k=10):
        sim = self.vsm.calculate_cosine_similarity(query_tfidf, matching='name')
        top_k_idx = self.get_top_k_index(sim, k)
        name_top_k = {}
        for idx in top_k_idx:
            name_top_k[idx] = [-1, sim[idx]]
        return name_top_k

    @staticmethod
    def calculate_api_score(api_text_top_k, api_name_top_k, k=10):
        for idx, v in api_name_top_k.items():
            if idx in api_text_top_k.keys():
                api_text_top_k[idx][1] = v[1]
            else:
                api_text_top_k[idx] = v
        api_top_k = api_text_top_k

        min_overlap_score = float('inf')
        max_not_overlap_score = -float('inf')
        for idx, v in api_top_k.items():
            if v[0] != -1 and v[1] != -1:
                min_overlap_score = min(v[0] + v[1], min_overlap_score)
            else:
                max_not_overlap_score = max(v[0], v[1], max_not_overlap_score)

        a = 0.1
        api_score = []  # 二维数组，第一维是api在method_list中的索引，第二维是api score
        for idx, v in api_top_k.items():
            if v[0] != -1 and v[1] != -1:
                api_score.append([idx, v[0] + v[1]])
            elif v[0] == -1:
                if min_overlap_score == float('inf'):
                    api_score.append([idx, v[1]])
                else:
                    api_score.append([idx, min_overlap_score * v[1] / (max_not_overlap_score + a)])
            else:
                if min_overlap_score == float('inf'):
                    api_score.append([idx, v[0]])
                else:
                    api_score.append([idx, min_overlap_score * v[0] / (max_not_overlap_score + a)])

        api_score = sorted(api_score, key=lambda x: x[1], reverse=True)
        return api_score[:k]

    def identify_relevant_api(self, query, k=10):
        query_tokens = self.preprocessing(query)
        query_tfidf = self.vsm.get_tfidf(query_tokens)
        api_text_top_k = self.text_matching(query_tfidf, k)
        api_name_top_k = self.name_matching(query_tfidf, k)
        api_top_k = self.calculate_api_score(api_text_top_k, api_name_top_k, k)
        return api_top_k

    def build_model(self, api_doc_file):
        with jl.open(api_doc_file) as reader:
            text_corpus = []
            name_corpus = []
            for method in reader:
                summary = self.preprocessing(method['summary'])
                remarks = self.preprocessing(method['remarks'])
                fqn = self.preprocessing(method['fqn'])
                text_corpus.append(summary + remarks)
                name_corpus.append(fqn)
                self.method_list.append({'fqn': fqn, 'summary': summary, 'remarks': remarks})
        self.vsm.build_tfidf_model(text_corpus, name_corpus)


if __name__ == '__main__':
    au = ApiUnderstanding()

    query = 'get network interface'
    api_top_k = au.identify_relevant_api(query)
    print(api_top_k)
    print('query [%s]: ' % query)
    [print(au.method_list[x[0]]['fqn']) for x in api_top_k]

    query = 'convert int to string'
    api_top_k = au.identify_relevant_api(query)
    print(api_top_k)
    print('query [%s]: ' % query)
    [print(au.method_list[x[0]]['fqn']) for x in api_top_k]

    query = 'string to date'
    api_top_k = au.identify_relevant_api(query)
    print(api_top_k)
    print('query [%s]: ' % query)
    [print(au.method_list[x[0]]['fqn']) for x in api_top_k]
